﻿using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners.FixtureData;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly FixturePartnersControllerBuilder _partnerControllerBuilder;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            _partnerControllerBuilder = new FixturePartnersControllerBuilder();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_LimitIsZero_ReturnsBadRequest()
        {
            // Arrange
            var partnerId = Guid.NewGuid();
            var promoCodeLimitRequest = FakeDataPartnersController.CreatePromoCodeLimitRequest(0);

            var partnersController = _partnerControllerBuilder.Build();

            // Act
            var result = await partnersController.SetPartnerPromoCodeLimitAsync(partnerId, promoCodeLimitRequest)
                as BadRequestObjectResult;

            // Assert
            // Лимит должен быть больше 0, иначе ошибка 400;
            result.Should().BeAssignableTo<BadRequestObjectResult>();
            result.Value.Should().Be("Лимит должен быть больше 0");
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotFound_ReturnsNotFound()
        {
            // Arrange
            var partnerId = Guid.NewGuid();
            var promoCodeLimitRequest = FakeDataPartnersController.CreatePromoCodeLimitRequest();

            var partnersController = _partnerControllerBuilder
                .FreezePartnerRepositoryGetByIdAsync(partnerId,null)
                .Build();

            // Act
            var result = await partnersController.SetPartnerPromoCodeLimitAsync(partnerId, promoCodeLimitRequest);

            // Assert
            // Если партнер не найден, тогда ошибка 404;
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotActive_ReturnsBadRequest()
        {
            // Arrange
            var partner = FakeDataPartnersController.CreateBasePartner(false);
            var promoCodeLimitRequest = FakeDataPartnersController.CreatePromoCodeLimitRequest();

            var partnersController = _partnerControllerBuilder
                .FreezePartnerRepositoryGetByIdAsync(partner.Id, partner)
                .Build();

            // Act
            var result = await partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, promoCodeLimitRequest)
                 as BadRequestObjectResult;

            // Assert
            // Если партнер заблокирован (partner.IsActive = false), тогда ошибка 400;
            result.Should().BeAssignableTo<BadRequestObjectResult>();
            result.Value.Should().Be("Данный партнер не активен");
        }

        # region Сравнение быстродействия между AutoFixture и базовыми возможностями языка
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_SetLimit_NumberIssuedPromoCodesShouldBeZero()
        {
            // Arrange
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            fixture.Behaviors
                .OfType<ThrowingRecursionBehavior>().ToList()
                .ForEach(t => fixture.Behaviors.Remove(t));
            fixture.Behaviors.Add(new OmitOnRecursionBehavior(recursionDepth: 1));

            var partner = fixture.Build<Partner>()
                .With(p => p.IsActive, true)
                .With(p => p.NumberIssuedPromoCodes, 100)
                .With(p => p.PartnerLimits, new List<PartnerPromoCodeLimit>())
                .Create();
            var partnerLimit = fixture.Build<PartnerPromoCodeLimit>()
                .With(p => p.CancelDate, (DateTime?)null)
                .With(p => p.PartnerId, partner.Id)
                .Create();
            var promoCodeLimitRequest = fixture.Create<SetPartnerPromoCodeLimitRequest>();

            fixture.Freeze<Mock<IRepository<Partner>>>()
                .Setup(r => r.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);
            fixture.Freeze<Mock<IRepository<PartnerPromoCodeLimit>>>()
                .Setup(r => r.FirstOrDefaultAsync(It.IsAny<Expression<Func<PartnerPromoCodeLimit, bool>>>()))
                .ReturnsAsync(partnerLimit);
            var partnersController = fixture.Build<PartnersController>()
                .OmitAutoProperties()
                .Create();

            // Act
            var result = await partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, promoCodeLimitRequest)
                as CreatedAtActionResult;
            
            // Assert
            result.RouteValues["id"].Should().Be(partner.Id);
            // При установке лимита для партнера, количество выпущенных промокодов обнуляется (partner.NumberIssuedPromoCodes = 0)
            partner.NumberIssuedPromoCodes.Should().Be(0);
        }

        // Выполняется в разы быстрее чем SetPartnerPromoCodeLimitAsync_SetLimit_NumberIssuedPromoCodesShouldBeZero
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_SetLimit_NumberIssuedPromoCodesShouldBeZero_2()
        {
            // Arrange
            var partner = new Partner()
            {
                Id = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8"),
                Name = "Суперигрушки",
                IsActive = true,
                NumberIssuedPromoCodes = 100,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
                {
                    new PartnerPromoCodeLimit()
                    {
                        Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                        PartnerId = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8"),
                        CreateDate = new DateTime(2020, 07, 9),
                        EndDate = new DateTime(2020, 10, 9),
                        Limit = 100,
                        CancelDate = null
                    }
                }
            };
            var partnerLimit = partner.PartnerLimits.First();
            var promoCodeLimitRequest = new SetPartnerPromoCodeLimitRequest()
            {
                EndDate = DateTime.Now.AddDays(10),
                Limit = 100
            };

            var partnerRepository = new InMemoryRepository<Partner>(
                new List<Partner> { partner });
            var partnerPromoCodeLimitRepository = new InMemoryRepository<PartnerPromoCodeLimit>(
                new List<PartnerPromoCodeLimit> { partnerLimit });
            var partnersController = new PartnersController(partnerRepository, partnerPromoCodeLimitRepository);

            // Act
            var result = await partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, promoCodeLimitRequest)
                as CreatedAtActionResult;

            // Assert
            result.RouteValues["id"].Should().Be(partner.Id);
            // При установке лимита для партнера, количество выпущенных промокодов обнуляется (partner.NumberIssuedPromoCodes = 0)
            partner.NumberIssuedPromoCodes.Should().Be(0);
        }
        #endregion

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_SetLimit_NumberIssuedPromoCodesShouldNotBeReset()
        {
            // Arrange
            var partner = FakeDataPartnersController.CreateBasePartner();
            var numberIssuedPromoCodes = partner.NumberIssuedPromoCodes;
            var promoCodeLimitRequest = FakeDataPartnersController.CreatePromoCodeLimitRequest();

            var partnersController = _partnerControllerBuilder
                .FreezePartnerRepositoryGetByIdAsync(partner.Id, partner)
                .FreezePartnerPromoCodeLimitRepositoryFirstOrDefaultAsync(null)
                .Build();

            // Act
            var result = await partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, promoCodeLimitRequest)
                as CreatedAtActionResult;

            // Assert
            result.RouteValues["id"].Should().Be(partner.Id);
            // При отсутствии лимита для партнера, количество выпущенных промокодов не должно обнуляться (partner.NumberIssuedPromoCodes)
            partner.NumberIssuedPromoCodes.Should().Be(numberIssuedPromoCodes);
        }
        
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_SetLimit_PreviousLimitDisabled()
        {
            // Arrange
            var partner = FakeDataPartnersController.CreateBasePartner();
            var partnerLimit = partner.PartnerLimits.First();
            var promoCodeLimitRequest = FakeDataPartnersController.CreatePromoCodeLimitRequest();

            var partnersController = _partnerControllerBuilder
                .FreezePartnerRepositoryGetByIdAsync(partner.Id, partner)
                .FreezePartnerPromoCodeLimitRepositoryFirstOrDefaultAsync(partnerLimit)
                .Build();

            // Act
            var result = await partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, promoCodeLimitRequest)
                 as CreatedAtActionResult;

            // Assert
            result.RouteValues["id"].Should().Be(partner.Id);
            // При установке лимита предыдущий лимит должен быть отключен (partnerLimit.CancelDate != null)
            partnerLimit.CancelDate.Should().NotBeNull();
        }
        
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_SetLimit_LimitIsSavedToDB()
        {
            // Arrange
            var partner = FakeDataPartnersController.CreateBasePartner();
            var promoCodeLimitRequest = FakeDataPartnersController.CreatePromoCodeLimitRequest(200);

            var options = new DbContextOptionsBuilder<DataContext>()
                .UseInMemoryDatabase(databaseName: "promocode_factory_db")
                .Options;

            using var dbContext = new DataContext(options);

            var partnerRepository = new EfRepository<Partner>(dbContext);
            await partnerRepository.AddAsync(partner);
            var partnerPromoCodeLimitRepository = new EfRepository<PartnerPromoCodeLimit>(dbContext);
            var partnersController = new PartnersController(partnerRepository, partnerPromoCodeLimitRepository);

            // Act
            var result = await partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, promoCodeLimitRequest)
                as CreatedAtActionResult;

            //Assert
            result.RouteValues["id"].Should().Be(partner.Id);

            // Проверка сохранения нового лимита в базе данных
            var partnerVerifiable = await partnerRepository.GetByIdAsync(partner.Id);
            var newLimit = partnerVerifiable.PartnerLimits.FirstOrDefault(p => !p.CancelDate.HasValue);

            newLimit.Limit.Should().Be(promoCodeLimitRequest.Limit);
            newLimit.EndDate.Should().Be(promoCodeLimitRequest.EndDate);
        }
    }
}