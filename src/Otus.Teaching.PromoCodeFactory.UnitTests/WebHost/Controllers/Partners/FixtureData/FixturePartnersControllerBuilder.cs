﻿using AutoFixture;
using AutoFixture.AutoMoq;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners.FixtureData
{
    /// <summary>
    /// строитель для PartnersController c подменой объектов
    /// </summary>
    public class FixturePartnersControllerBuilder
    {
        private IFixture _fixture;

        public FixturePartnersControllerBuilder()
        {
            _fixture = new Fixture().Customize(new AutoMoqCustomization());

            _fixture.Behaviors
                .OfType<ThrowingRecursionBehavior>().ToList()
                .ForEach(t => _fixture.Behaviors.Remove(t));
            _fixture.Behaviors.Add(new OmitOnRecursionBehavior(recursionDepth: 1));
        }

        public FixturePartnersControllerBuilder FreezePartnerRepositoryGetByIdAsync(Guid id, Partner result)
        {
            _fixture.Freeze<Mock<IRepository<Partner>>>()
                .Setup(r => r.GetByIdAsync(id))
                .ReturnsAsync(result);

            return this;
        }

        public FixturePartnersControllerBuilder FreezePartnerPromoCodeLimitRepositoryFirstOrDefaultAsync(PartnerPromoCodeLimit result)
        {
            _fixture.Freeze<Mock<IRepository<PartnerPromoCodeLimit>>>()
                .Setup(r => r.FirstOrDefaultAsync(It.IsAny<Expression<Func<PartnerPromoCodeLimit, bool>>>()))
                .ReturnsAsync(result);

            return this;
        }

        public PartnersController Build()
        {
            return _fixture.Build<PartnersController>()
                .OmitAutoProperties() // отключить функцию автогенерации свойств
                .Create();
        }
    }
}
