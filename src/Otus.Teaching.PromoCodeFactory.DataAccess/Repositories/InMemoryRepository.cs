﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data.AsEnumerable());
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<T> FirstOrDefaultAsync(Expression<Func<T, bool>> predicate)
        {
            return Task.FromResult(Data.FirstOrDefault(predicate.Compile()));
        }

        public Task<IEnumerable<T>> GetRangeByIdsAsync(List<Guid> ids)
        {
            return Task.FromResult(Data.Where(x => ids.Contains(x.Id)).AsEnumerable());
        }

        public Task AddAsync(T entity)
        {
            entity.Id = Guid.NewGuid();
            Data = Data.Concat(new[] { entity });

            return Task.FromResult(entity.Id);
        }

        public Task UpdateAsync(T entity)
        {
            Data = Data.Where(e => e.Id != entity.Id);
            Data = Data.Concat(new[] { entity });
            return Task.CompletedTask;
        }

        public Task DeleteAsync(T entity)
        {
            Data = Data.Where(e => e.Id != entity.Id);
            return Task.CompletedTask;
        }
    }
}